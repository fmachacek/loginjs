V této úloze ozkouší řešitel své schopnosti psaní scriptů za pochodu. "Rezavá" stránka, která ukazuje zda-li je přihlašovací jméno správné/špatné, a podivný script běžící v pozadí stránky. Řešitel zjistí, že heslo musí být větší než 10 charakterů,
ale menší než 20, také zjistí, že součet ASCII hodnot všech charakterů hesla se rovná 428. Má dvě možnosti; napsat script, který najde heslo za něj, nebo stará známá metoda - pokus omyl. Po zadání správného hesla se zobrazí flag.

flag: flag{pivocas}